# ph-converter: Converts any blocklist into a Pi-Hole compatible format
This script can take any blocklist, like the ones used from uBlock origin or AdBlock Plus, and convert it to a Pi-Hole Blocklist. That means in the end you will get a file with only domains left.

**Important:** Currently I suspect that more is removed from the file than it should. So keep in mind that there may be less domains on the block list than you expect.

## Usage
Use the script by executing:

`ph-converter blocklist-you-want-to-convert.txt blocklist-output-file.txt`

* `blocklist-you-want-to-convert.txt` has to be replaced with the blocklist file that you want to convert.
* `blocklist-output-file.txt` is the Pi-Hole compatible blocklist file that will be created. You can use whatever name you want.
