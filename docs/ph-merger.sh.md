# ph-merger: Pi-Hole blacklist merger
This script merges existing pi-hole blocklist files into one file with only uniq domains left.

## Usage
Use the script by executing:

`ph-merger blocklist-files-list.txt blocklist-files-merged.txt whitelist.txt`

* `blocklist-files-list.txt` has to be replaced with a file which has to contain the URLs to the host files you want to merge into one file. One URL per line.
* `blocklist-files-merged.txt` is the file where all the hosts are merged to. You can use whatever name you want.
* `whitelist.txt` is the name of the file which contain the sed-scripts for whitelisted domains that will be removed from the blocklist

That's it! As result you will get one host file, with only domains and no duplicates in it.
