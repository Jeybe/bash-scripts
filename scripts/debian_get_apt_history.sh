#!/bin/bash
# Copied from:
# https://github.com/blyork/apt-history

function echoHistory(){
    if [[ $1 == *.gz ]] ; then
        gzip -cd $1
    else
        if [[ $1 == *.log || $1 == *.log.1 ]] ; then
            cat $1
        else
            echo "\*\*\* Invalid File: ${1} \*\*\*" 1>&2
        fi
    fi
}

FILES=( `ls -rt /var/log/dpkg.log*` ) || exit 1

for file in "${FILES[@]}"
do
    case "$1" in
        install)
            echoHistory $file | grep " install "
        ;;

        upgrade|remove)
            echoHistory $file | grep " ${1} "
        ;;

        rollback)
            echoHistory $file | grep upgrade | \
                grep "$2" -A10000000 | \
                grep "$3" -B10000000 | \
                awk '{print $4"="$5}'
        ;;

        list)
            echoHistory $file
        ;;

        *)
            echo "Parameters:"
            echo "     install          - Lists all packages that have been installed."
            echo "     upgrade          - Lists all packages that have been upgraded."
            echo "     remove           - Lists all packages that have been removed."
            echo "     rollback         - Lists rollback information."
            echo "     list             - Lists all contents of dpkg logs."
            break
        ;;
    esac
done

exit 0

