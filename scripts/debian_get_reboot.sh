#!/bin/bash
if [[ -f /var/run/reboot-required ]]
then
  echo "Reboot needed!"
  echo "Please restart the following:"
  echo "-----------------------------"
  echo "System"
elif [[ -f /var/run/reboot-required.pkgs ]]
then
  echo "Reboot needed!"
  echo "Please restart the following:"
  echo "-----------------------------"
  cat /var/run/reboot-required.pkgs
else
  echo "No reboot needed."
fi
