#!/bin/bash
# von https://privacy-handbuch.de/handbuch_31l.htm

# Check des SMTP Servers
danetool --check posteo.de --port 465
if [ $? -ne 0 ]; then
    # SSL Zertifikatsfehler! Warnung und Abbruch
    kdialog --error "DANE/TLSA Fehler bei SMTP Server!"
    exit 0
fi

# Check des IMAP Servers
danetool --check posteo.de --port 993
if [ $? -ne 0 ]; then
    # SSL Zertifikatsfehler! Warnung und Abbruch
    kdialog --error "DANE/TLSA Fehler bei IMAP Server!"
    exit 0
fi

# alles ok, Thunderbird kann starten
thunderbird 
