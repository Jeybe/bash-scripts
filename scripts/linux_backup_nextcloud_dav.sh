#!/bin/bash
if [[ $1 && $2 && $3 && $4 && $5 && $6 ]]; then
	if [[ $1 = "-d" ]]; then
		DOMAIN=$2
	elif [[ $3 = "-d"  ]]; then
		DOMAIN=$4
	elif [[ $5 = "-d" ]]; then
		DOMAIN=$6
	else
		DOMAIN=-1
	fi

	if [[ $1 = "-u" ]]; then
        	USER=$2
	elif [[ $3 = "-u"  ]]; then
        	USER=$4
	elif [[ $5 = "-u" ]]; then
        	USER=$6
	else
		USER=-1
	fi

	if [[ $1 = "-c" ]]; then
        	CALENDAR=$2
	elif [[ $3 = "-c"  ]]; then
        	CALENDAR=$4
	elif [[ $5 = "-c" ]]; then
        	CALENDAR=$6
	else
		CALENDAR=-1
	fi
	read -s -p "Password: " password
	echo "\n"
	wget -q --output-document ./calendar.ics --auth-no-challenge --http-user=$USER --http-password=$password "https://$DOMAIN:443/remote.php/dav/calendars/$USER/$CALENDAR?export"
else
	echo "Bitte erforderliche Parameter angeben"
fi


