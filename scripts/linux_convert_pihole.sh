#!/bin/bash

# Create a temporary copy to work with
cp $1 .tmp-blocklist.txt

# Remove comments: ## # ! .
sed -i '/^\#/d; s/[[:space:]]\#.*$//g; /^\!/d; s/[[:space:]]\!.*$//g; /^\./d; /\.$/d' .tmp-blocklist.txt

# Remove content filters rules and ip addresses
sed -i '/^https\:\/\//d; /^http\:\/\//d; s/[\^]third-party$//g; s/[\^]popup$//g; s/[\^]important$//g; s/[\^]subdocument$//g; s/[\^]websocket$//g; s/^||//; s/[\^]$//g' .tmp-blocklist.txt
sed -i 's/^PRIMARY\s\+[ \t]*//g; s/^localhost\s\+[ \t]*//g; s/blockeddomain.hosts\s\+[ \t]*//g; s/^0.0.0.0\s\+[ \t]*//g; s/^127.0.0.1\s\+[ \t]*//g; s/^::1\s\+[ \t]*//g' .tmp-blocklist.txt

# Remove empty space
sed -i 's/^\s\+[ \t]//g; s/\s\+[ \t]$//g; /^\s*$/d; / /d' .tmp-blocklist.txt

# Remove invalid characters
sed -i '/[][`~!@#$%^&*()=+{}\";:?/><,|]/d; /'\''/d' .tmp-blocklist.txt

# Convert everything to lowercase
sed -i 's/\([A-Z]\)/\L\1/g' .tmp-blocklist.txt

# Remove everything left which isn't a period or a letter
sed -i '/[a-z]/!d; /[.]/!d; /[a-z]$/!d; /^\./d; /\.$/d; /^.\{,255\}$/!d;/^-/ d' .tmp-blocklist.txt

# Creating output file and removing the temporary copy
cp .tmp-blocklist.txt $2
rm .tmp-blocklist.txt
