#!/bin/bash

# Attempt to remove already existing, merged blocklist
rm $2

# Download and merge blocklist files
#wget -nv -i $1 -P .host-files/ &&  sort -f -u -o $2 .host-files/* && rm -R .host-files/
while read -r line
do
	wget -T 5 -nv $line -P .host-files/ 
done < $1
sort -f -u -o $2 .host-files/*
rm -R .host-files/

# Remove entries from whitelist
sed -i -f $3 $2

# Remove IP addresses, comments, empty space, empy lines and duplicates
sed -i 's/0\.0\.0\.0//g' $2 && sed -i 's/127\.0\.0\.1//g' $2 && sed -i '/^#/d' $2 && sed -i 's/[[:space:]]//g' $2 && sed -i '/^$/d'
sort -f -u -o $2 $2

# Remove entries from whitelist
sed -i -f $3 $2

