#!/bin/bash

# ------ INIT, CLEAR FILES-------
rm -f .tmp.txt
rm -f .tmp2.txt
rm -f .tmp3.txt
rm -f .tmp4.txt
rm -f .tmp5.txt
rm -f .tmp6.txt

# ------- EXTRACT NECCESSARY INFO - PART 1 --------
# read file and remove path per line
cat $1 | while read line; do echo ${line##*/} >> .tmp.txt; done;
# remove comments
sed -i '/^\#/d; s/[[:space:]]\#.*$//g; /^\!/d; s/[[:space:]]\!.*$//g; /^\./d; /\.$/d' .tmp.txt
# remove everything other than .ts filenames
grep -E '*.ts' .tmp.txt > .tmp2.txt
# remove lines with _ (ad files)
grep -v "_" .tmp2.txt > .tmp3.txt
# remove ending, numbers and -
cat .tmp3.txt | while read line; do echo ${line%-*} >> .tmp4.txt; done;

# --------- EXTRACT NECCESSARY INFO - PART 2 --------
# write lines in array
i=0
while read line
do
    filelines[i]=$line
    ((i++))
done < .tmp4.txt
# extract beginning of numbered filenames
len=${#filelines[@]} 
for ((j=0;j<len;j++))
do
    l=$(($j+1))
    tmp1=${filelines[j]}
    tmp2=${filelines[l]}
    if [ "$tmp1" != "$tmp2" ]
      then
        echo $tmp1 >> .tmp5.txt
    fi    
done

# ---------- MERGE FILES ------------
# put files together
while read line
do
    for ((m=1;m<100;m++))
    do
        cat $line-$m.ts >> ${1%%.*}.ts
    done    
done < .tmp5.txt

