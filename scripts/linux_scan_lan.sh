#!/bin/bash
clear
echo "|||||||||||||||||||||||||||||||||||||||||"
echo "|||||||||||||||||||||||||||||||||||||||||"
echo "||           LAN SCANNER               ||"
echo "|||||||||||||||||||||||||||||||||||||||||"
echo "|||||||||||||||||||||||||||||||||||||||||"
echo ""
echo "Scanning you network. Depending on your setup this proccess can last up to a few minutes."
echo "Normally it should not take longer than one minute and become faster from the second run." 

# --- detect network part of local network --

# gain info about localhost
localHostname=$(hostname)
lastLine=$(host $localHostname | tr " " "\n" | wc -l)
localIPv4Address=$(host $localHostname | tr " " "\n" | sed "$lastLine q;d")
publicIPv4Address=$(curl -s https://4.ipwho.de/json | tr "," "\n" | grep -e '\"ip\"' | tr "\"" "\n" | sed '4q;d')
publicIPv6Address=$(curl -s https://6.ipwho.de/json | tr "," "\n" | grep -e '\"ip\"' | tr "\"" "\n" | sed '4q;d')
netpartOfLocalIPv4Address=${localIPv4Address%.*}

# --- prepare storage ---

datadir=".lan-scanner"

if [ ! -d $datadir ]; then
	mkdir $datadir
fi
cd $datadir

known=".known-hosts"
missing=".missing-hosts"
outputLocal=".local-host.scan"
outputKnown=".known-hosts.scan"
outputMissing=".missing-hosts.scan"

if [[ ! -f $known ]]; then
  touch $known
fi

if [[ ! -f $missing ]]; then
  touch $missing
  echo -e '\n'$netpartOfLocalIPv4Address.{1..254} >> $missing
  sed -i '/^$/d' $missing
fi

echo "IP-Address Status Hostname" >  $outputKnown
echo "IP-Address Status Hostname" > $outputMissing

if [[ -f $outputLocal ]]; then
  rm $outputLocal && touch $outputLocal
fi

# --- store info about localhost ---

# store info about localhost
if [ ! $localHostname ]; then
  echo "Local hostname: None" >> $outputLocal
else
  echo "Local hostname: "$localHostname >> $outputLocal
fi

if [ ! $localIPv4Address ]; then
  echo "Local IPv4-Address: None" >> $outputLocal
else
  echo "Local IPv4-Address: "$localIPv4Address >> $outputLocal
fi

if [ ! $publicIPv4Address ]; then
  echo "Public IPv4-Address: None" >> $outputLocal
else
  echo "Public IPv4-Address: "$publicIPv4Address >> $outputLocal
fi

if [ ! $publicIPv6Address ]; then
  echo "Public IPv6-Address: None" >> $outputLocal
else
  echo "Public IPv6-Address: "$publicIPv6Address >> $outputLocal
fi

# --- scan network ---

# check known hosts and store info
while read -r line
do
  timeout 0.2 ping -c 1 $line > /dev/null
  hostHostname=$(host $line | tr " " "\n" | sed '5q;d')
  if [ "$hostHosname" == "3(NXDOMAIN)" ]; then
	  hostHostname="No hostname"
  fi
  if [ $? -ne 124 ]; then 
    echo $line" up "$hostHostname >> $outputKnown
  else 
    echo $line" down "$hostHostname >> $outputKnown
  fi
done < $known

# check all other ip-addresses and store info
while read -r line
do
  timeout 0.2 ping -c 1 $line > /dev/null
 # hostTwoHostname=$(host $line | tr " " "\n" | sed '5q;d')
  #hostHostname=$(host $line | tr " " "\n" | sed '5q;d')
  if [ $? -ne 124 ]; then
    echo $line" up "$hostTwoHostname >> $outputMissing
    echo $line >> $known 
    sed -i '/'$line'/d' $missing
  fi
done < $missing


# --- rendering output ----

# rendering info about localhost
echo ""
echo "|--------------------------------|"
echo "|       System information       |"
echo "|--------------------------------|"
echo ""
column -t -s ' ' $outputLocal

# rendering info about known hosts
echo ""
echo "|--------------------------------|"
echo "|          Known hosts           |"
echo "|--------------------------------|"
echo ""
column -t -s ' ' $outputKnown

# rendering info about new hosts
echo ""
echo "|--------------------------------|"
echo "|          New hosts             |"
echo "|--------------------------------|"
echo ""
if [ $(cat $outputMissing | wc -l) -gt 1 ]; then
  column -t -s ' ' $outputMissing
else
  echo "No new hosts detected."
fi
echo " "
