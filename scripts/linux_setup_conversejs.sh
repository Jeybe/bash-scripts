#!/bin/bash
################################
### linux install conversejs ###
################################

echo "Detecting latest ConverseJS release..."
conversejs_github_repo="https://github.com/conversejs/converse.js"
conversejs_latest_release_page=$(curl -Ls -o /dev/null -w %{url_effective} "$conversejs_github_repo/releases/latest")
conversejs_latest_version_number=${conversejs_latest_release_page##*/}
conversejs_latest_release_pkg="$conversejs_github_repo/releases/download/$conversejs_latest_version_number/converse.js-$conversejs_latest_version_number.tgz"
echo "Downloading ConverseJS $conversejs_latest_version_number"
wget -O /tmp/conversejs_latest.tar.gz $conversejs_latest_release_pkg
tar -xzf /tmp/conversejs_latest.tar.gz -C /tmp/
wget -O /tmp/conversejs_config https://codeberg.org/spootle/conversejs/raw/branch/main/conversejs.config.js
wget -O /tmp/conversejs_index https://codeberg.org/spootle/conversejs/raw/branch/main/index.html
sudo cp -r "/tmp/package" "/var/www/conversejs"
sudo cp "/tmp/conversejs_config" "/var/www/conversejs/conversejs.config.js"
sudo cp "/tmp/conversejs_index" "/var/www/conversejs/index.html"
sudo chown -R www-data:www-data /var/www/conversejs
