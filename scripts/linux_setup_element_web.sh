#!/bin/bash
##################################
### linux install element web ###
##################################

echo "Detecting latest Element Web release..."
element_web_github_repo="https://github.com/vector-im/element-web"
element_web_latest_release_page=$(curl -Ls -o /dev/null -w %{url_effective} "$element_web_github_repo/releases/latest")
element_web_latest_version_number=${element_web_latest_release_page##*/}
element_web_latest_release_pkg="$element_web_github_repo/releases/download/$element_web_latest_version_number/riot-$element_web_latest_version_number.tar.gz"
echo "Downloading Element Web $element_web_latest_version_number"
wget -O /tmp/element-web-latest.tar.gz $element_web_latest_release_pkg
tar -xzf /tmp/element-web-latest.tar.gz -C /tmp/
wget -O /tmp/element_web_config.json https://codeberg.org/spootle/element_web/raw/branch/main/config.json
sudo cp -r "/tmp/riot-$element_web_latest_version_number" "/var/www/element_web"
sudo cp "/tmp/element_web_config.json" "/var/www/element_web/config.json"
sudo chown -R www-data:www-data /var/www/element_web
